package de.schildbach.oeffi;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.zookey.universalpreferences.UniversalPreferences;

public class SecretSecrets {

    private static AdView mAdView;
    public static void zzbb(Activity activity) {
        mAdView = activity.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();

        Log.i("SecretSecrets", "Wirkliche Appstarts: " + UniversalPreferences.getInstance().get("app_starts", 0) + ". AppStarts nötig für Werbung: " + FirebaseConfigurator.getAppStartAds()+". Footer Werbung angeschaltet? "+ FirebaseConfigurator.showAdsFooter());
        if((int) UniversalPreferences.getInstance().get("app_starts", 0) >= FirebaseConfigurator.getAppStartAds() && FirebaseConfigurator.showAdsFooter()) {
            mAdView.loadAd(adRequest);
            Log.i("SecretSecrets", "Werbung wird gezeigt!");
            if((int) UniversalPreferences.getInstance().get("app_starts", 0) == FirebaseConfigurator.getAppStartAds()) {
                FirebaseConfigurator.sendEvent(activity, "benutzer_sieht_jetzt_werbung");
            }
        }
        else {
            Log.i("SecretSecrets", "Werbung wird nicht gezeigt!");
        }

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                mAdView.setVisibility(View.GONE);
            }
        });
    }
}


