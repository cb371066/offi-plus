package de.schildbach.oeffi;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import androidx.annotation.NonNull;

public class FirebaseConfigurator {

    private static FirebaseRemoteConfig mFirebaseRemoteConfig;
    private Activity activity;

    public static void fetcher(Activity activity){
        // Get Remote Config instance.
        // [START get_remote_config_instance]
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        // [END get_remote_config_instance]

        // Create a Remote Config Setting to enable developer mode, which you can use to increase
        // the number of fetches available per hour during development. See Best Practices in the
        // README for more information.
        // [START enable_dev_mode]
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        // [END enable_dev_mode]

        // Set default Remote Config parameter values. An app uses the in-app default values, and
        // when you need to adjust those defaults, you set an updated value for only the values you
        // want to change in the Firebase console. See Best Practices in the README for more
        // information.
        // [START set_default_values]
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_default);
        // [END set_default_values]
        long cacheExpiration = 3600; // 1 hour in seconds.
        // If your app is using developer mode, cacheExpiration is set to 0, so each fetch will
        // retrieve values from the service.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        // [START fetch_config_with_callback]
        // cacheExpirationSeconds is set to cacheExpiration here, indicating the next fetch request
        // will use fetch data from the Remote Config service, rather than cached parameter values,
        // if cached parameter values are more than cacheExpiration seconds old.
        // See Best Practices in the README for more information.
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // After config data is successfully fetched, it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();
                        }
                    }
                    // [END fetch_config_with_callback]
                });
        String temp = mFirebaseRemoteConfig.getString("citytraffic_app_start_rating");
        Log.d("Firebase", "Firebase Remote Config. app_start_rating: "+temp);
        String temp1 = mFirebaseRemoteConfig.getString("citytraffic_app_start_ads");
        Log.d("Firebase", "Firebase Remote Config. app_start_ads: "+temp1);
    }

    public static int getAppStartAds(){
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        return Integer.valueOf(mFirebaseRemoteConfig.getString("citytraffic_app_start_ads"));
    }

    public static int getAppStartRating(){
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        return Integer.valueOf(mFirebaseRemoteConfig.getString("citytraffic_app_start_rating"));
    }

    public static boolean showAdsFooter() {
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        return Boolean.valueOf(mFirebaseRemoteConfig.getString("citytraffic_ads_footer_visible"));
    }

    public static boolean showAdsInterstitial() {
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        return Boolean.valueOf(mFirebaseRemoteConfig.getString("citytraffic_ads_interstitial_visible"));
    }

    public static String getAdmob_clientID() {
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        return mFirebaseRemoteConfig.getString("citytraffic_admob_client_id");
    }

    //2 Unterkategorien
    public static void sendEvent(Activity activity, String category, String parameterMayor, String parameterMayorValue, String parameterMinor, String parameterMinorValue){
        Bundle bundle = new Bundle();
        bundle.putString(parameterMinor, parameterMinorValue);
        bundle.putString(parameterMayor, parameterMayorValue);
        try {
            FirebaseAnalytics.getInstance(activity).logEvent(category, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //1 Unterkategorie
    public static void sendEvent(Activity activity, String category, String parameterMayor, String parameterMayorValue){
        Bundle bundle = new Bundle();
        bundle.putString(parameterMayor, parameterMayorValue);
        try {
            FirebaseAnalytics.getInstance(activity).logEvent(category, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //0 Unterkategorie
    public static void sendEvent(Activity activity, String category){
        Bundle bundle = new Bundle();
        try {
            FirebaseAnalytics.getInstance(activity).logEvent(category, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
